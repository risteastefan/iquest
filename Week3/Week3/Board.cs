﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week3
{
    class Board
    {
        private List<IShape> Shapes;
        public int FindTotalArea()
        {
            int totalArea = 0;
            foreach (var shape in Shapes)
                totalArea += shape.Area();
            return totalArea;
        }
        public Board()
        {
            Shapes = new List<IShape>();
        }
        public void addShape(Rectangle r)
        {
            Shapes.Add(r);
        }
        public void editShape(int index, Rectangle r)
        {
            Shapes[index] = new Rectangle(r);
        }
        public void removeShape(int index)
        {
            Shapes.RemoveAt(index);
        }
        public int nrOfShapes()
        {
            return Shapes.Count;
        }
        public int nrOfVertices()
        {
            return Shapes.Count * 4;
        }
    }
}
