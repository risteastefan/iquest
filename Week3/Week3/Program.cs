﻿using System;

namespace Week3
{
    class Program
    {
        static void Main(string[] args)
        {
            Board brd = new Board();
            brd.addShape(new Rectangle(0, 0, 3, 4));
            brd.addShape(new Rectangle(6, 7, 7, 4));
            brd.addShape(new Rectangle(20, 5, 8, 4));

            brd.editShape(1, new Rectangle(4, 10, 9, 5));
            brd.removeShape(2);

            Console.WriteLine(brd.nrOfShapes());
            Console.WriteLine(brd.FindTotalArea());

            Rectangle rect = new Rectangle(0, 0, 2, 3);
            Console.WriteLine(rect.Perimeter());
            Console.WriteLine(rect.Area());
        }
    }
}
