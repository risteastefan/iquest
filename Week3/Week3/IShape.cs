﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week3
{
    interface IShape
    {
        int Area();
        int Perimeter();
    }
}
