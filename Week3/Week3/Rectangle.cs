﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week3
{
    struct Point
    {
        public int X, Y;
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
    struct Rectangle : IShape
    {
        private Point Center;
        public int Width { private get; set; }
        public int Height { private get; set; }
        public Rectangle(int x, int y, int width, int height)
        {
            Center = new Point(x, y);
            Width = width;
            Height = height;
        }
        public Rectangle(Rectangle r)
        {
            Center = r.Center;
            Width = r.Width;
            Height = r.Height;
        }
        public int Area()
        {
            return Width * Height;
        }
        public int Perimeter()
        {
            return Width + Height;
        }
    }
}
