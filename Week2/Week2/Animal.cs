﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2
{
    abstract class Animal
    {
        protected string Name, Sound;

        public void MakeSound()
        {
            System.Console.WriteLine("{0} makes {1}", Name, Sound);
        }

        public abstract void DoMovement();
        protected Animal(string name)
        {
            Name = name;
        }
    }
}
