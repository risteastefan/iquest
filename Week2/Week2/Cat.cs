﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2
{
    class Cat : Animal
    {
        IMove JumpingMove = new JumpingAnimal();
        public Cat(string name) : base(name)
        {
            Sound = "Miau";
        }
        public override void DoMovement()
        {
            JumpingMove.Move(Name);
        }
    }
}
