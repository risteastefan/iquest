﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2
{
    class Dog : Animal
    {
        IMove runningMove = new RunningAnimal();
        public Dog(string name) : base(name)
        {
            Sound = "Woof";
        }
        public override void DoMovement()
        {
            runningMove.Move(Name);
        }
    }
}
