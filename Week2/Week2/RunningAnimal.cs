﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2
{
    class RunningAnimal : IMove
    {
        public void Move(string name)
        {
            Console.WriteLine("{0} runs", name);
        }
    }
}
