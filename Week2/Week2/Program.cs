﻿using System;
using System.Collections.Generic;

namespace Week2
{
    class Program
    {

        static void Main(string[] args)
        {
            var Animals = new List<Animal> {
                new Dog("Rex"),
                new Dog("Azorel"),
                new Dog("Iancu"),
                new Cat("Max"),
                new Alpaca("Pablo"),
                new Duck("Ducky")};
            
            foreach (var animal in Animals)
            {
                animal.MakeSound();
            }
            Console.WriteLine();
            foreach (var animal in Animals)
            {
                animal.DoMovement();
            }
        }
    }
}
