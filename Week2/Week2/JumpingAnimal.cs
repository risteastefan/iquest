﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2
{
    class JumpingAnimal : IMove
    {
        public void Move(string name)
        {
            Console.WriteLine("{0} jumps", name);
        }
    }
}
