﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2
{
    class Alpaca : Animal
    {
        IMove LevitatingMove = new LevitatingAnimal();
        public Alpaca(string name) : base(name)
        {
            Sound = "arghhh";
        }
        public override void DoMovement()
        {
            LevitatingMove.Move(Name);
        }
    }
}
