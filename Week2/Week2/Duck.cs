﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week2
{
    class Duck : Animal
    {
        IMove flyingMove = new FlyingAnimal();
        IMove runningMove = new JumpingAnimal();
        public Duck(string name) : base(name)
        {
            Sound = "Quack";
        }
        public override void DoMovement()
        {
            flyingMove.Move(Name);
            runningMove.Move(Name);
        }
    }
}
