﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week6
{
    class SelectableSize : ISizeType
    {
        public string SizeSelection()
        {
            List<string> sizeList = new List<string> { "Short", "Tall", "Grande", "Venti" };
            System.Console.WriteLine("\n\n\nWhat size do you want your coffee ?");
            System.Console.WriteLine("1. Short");
            System.Console.WriteLine("2. Tall");
            System.Console.WriteLine("3. Grande");
            System.Console.WriteLine("4. Venti");
            return sizeList[GetCommandClass.GetCommand() - 1];
        }
    }
}
