﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week6
{
    class Beverage
    {
        public Beverage(string name, ISizeType sizeType, ICoffeeDosage coffeeDosage) => (Name, SizeType, CoffeeDosage) = (name, sizeType, coffeeDosage);
        public void SetSize()
        {
            Size = SizeType.SizeSelection();
            Dosage = CoffeeDosage.DosageSelection();
        }
        public void setCoffeeDosage()
        {

        }
        public string Name { get; private set; }
        public string Size { get; private set; }
        public string Dosage { get; private set; }

        private ISizeType SizeType;
        private ICoffeeDosage CoffeeDosage;
    }
}
