﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week6
{
    class SelectableDosage : ICoffeeDosage
    {
        public string DosageSelection()
        {
            System.Console.WriteLine("\n\n\nDo you want stronger coffee or a more flavored one ?");
            System.Console.WriteLine("1. Strong");
            System.Console.WriteLine("2. Flavored");

            int command = GetCommandClass.GetCommand();
            if (command == 1)
                return "Strong";
            else
                return "Flavored";
        }
    }
}
