﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week6
{
    class FixedSizeShort : ISizeType
    {
        string ISizeType.SizeSelection()
        {
            System.Console.WriteLine("The size for this coffee is fixed : short");
            return "Short";
        }
    }
}
