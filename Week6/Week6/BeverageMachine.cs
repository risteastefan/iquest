﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week6
{
    class GetCommandClass
    {
        static public int GetCommand()
        {
            string commandCharacter;
            commandCharacter = Console.ReadLine();
            int command = Convert.ToInt32(commandCharacter);
            return command;
        }
    }
    public class BeverageMachine
    {
        public void Process()
        {
            int screenBCommand = 0;
            switch (ScreenA())
            {
                case 1:
                    screenBCommand = ScreenB();
                    break;
                case 2:
                    return;
            }
            ScreenC_D(BeverageNames[screenBCommand - 1]);
            ScreenE();
        }

        private List<string> BeverageNames = new List<string>
        {
            "Caramel Macchiato", "Caffe Latte", "Cappucinno", "Caffe Americano", "White Chocolate Mocha",
            "Caffe Mocha", "Chai Tea Latte", "Pumpkin Spice Latte", "Doppio Espresso Macchiato", "Espresso Shot"
        };

        private int ScreenB()
        {
            Console.WriteLine("\n\n\n\nWhat beverage do you want ?");
            int i = 0;
            foreach (string name in BeverageNames)
            {
                i++;
                Console.WriteLine("{0}. {1}", i, name);
            }
            return GetCommandClass.GetCommand();
        }

        private int ScreenA()
        {
            Console.WriteLine("1. Order a coffee");
            Console.WriteLine("2. Cancel");
            return GetCommandClass.GetCommand();
        }
        private void ScreenC_D(string name)
        {
            ISizeType sizeType;
            ICoffeeDosage dosageType;

            if (name == "Espresso Shot")
                sizeType = new FixedSizeShort();
            else
                sizeType = new SelectableSize();

            if (name == "Chai Tea Latte")
                dosageType = new NoDosage();
            else
                dosageType = new SelectableDosage();

            Beverage = new Beverage(name, sizeType, dosageType);
            Beverage.SetSize();
            Beverage.setCoffeeDosage();
        }

        private void ScreenE()
        {
            Console.WriteLine("\n\n\nHere's your beverage! Please enjoy and come again!\n{0}    {1}        {2}", Beverage.Name, Beverage.Size, Beverage.Dosage);
        }
        private Beverage Beverage;
    }
}
