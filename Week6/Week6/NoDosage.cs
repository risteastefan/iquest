﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week6
{
    class NoDosage : ICoffeeDosage
    {
        public string DosageSelection()
        {
            System.Console.WriteLine("\n\n\nYou cannot have coffee in this beverage");
            return "None";
        }
    }
}
