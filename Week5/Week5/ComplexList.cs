﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Week5
{
    public class ComplexList : IEnumerable
    {
        public void Add(Complex param)
        {
            Size++;
            if (Head == null)
            {
                Head = new Node(param);
            }
            else
            {
                Node curr = Head;
                while (curr.next != null)
                {
                    curr = curr.next;
                }
                curr.next = new Node(param);
            }
        }

        public void RemoveAt(int position)
        {
            if (position >= Size || position < 0)
            {
                System.Console.WriteLine("Cannot remove index. Out of bounds");
                return;
            }
            if (position == 0)
            {
                Head = Head.next;
            }
            else
            {
                Node curr = Head;
                int currPos = 1;
                while (currPos < position)
                {
                    currPos++;
                    curr = curr.next;
                }
                curr.next = curr.next.next;
            }
            Size--;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public ComplexListEnumerator GetEnumerator()
        {
            return new ComplexListEnumerator(Head);
        }
        public int Size { get; private set; }
        public Node Head { get; private set; }
    }
}
