﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week5
{
    public class Node
    {
        public Node(Complex value, Node next = null) => (data, this.next) = (value, next);
        private Complex data;
        public Node next;
        public Complex Data
        {
            get => data;
            private set => this.data = value;
        }
    }
}
