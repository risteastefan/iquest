﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Week5
{
    public class ComplexListEnumerator : IEnumerator
    {
        public ComplexListEnumerator(Node head)
        {
            this.head = new Node(null) ;
            this.head.next = head;
        }

        public bool MoveNext()
        {
            head = head.next;
            return head != null;
        }

        public void Reset()
        {
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public Complex Current
        {
            get
            {
                return head.Data;
            }
        }

        private Node head;
    }
}
