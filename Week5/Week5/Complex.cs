﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week5
{
    public class Complex
    {
        public Complex(int real = 0, int imag = 0) => (this.real, this.imag) = (real, imag);
        public int real, imag;

        public int Real
        {
            get => real;
            private set => real = value;
        }
        public int Imag
        {
            get => imag;
            private set => imag = value;
        }
    }
}
