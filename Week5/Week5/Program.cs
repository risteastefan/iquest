﻿using System;

namespace Week5
{
    class Program
    {
        static void Main()
        {


            ComplexList complexList = new ComplexList();
            Complex c1 = new Complex(1, 2);
            Complex c2 = new Complex(2, 3);
            Complex c3 = new Complex(3, 4);
            complexList.Add(c1);
            complexList.Add(c2);
            complexList.Add(c3);
            complexList.RemoveAt(2);
            complexList.RemoveAt(0);
            complexList.RemoveAt(0);
            foreach (Complex c in complexList)
                Console.WriteLine(c.Real + " + " + c.Imag + "i");
        }
    }
}
